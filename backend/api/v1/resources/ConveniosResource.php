<?php

namespace backend\api\v1\resources;
 
use backend\api\v1\models\Convenios;
use backend\api\v1\models\search\ConveniosSearch;
use tecnocen\roa\controllers\Resource;
use Yii;
use yii\helpers\VarDumper;
 
class ConveniosResource extends Resource
{
    public $idAttribute = 'id';
    /**
     * @inheritdoc
     */
    public $modelClass = Convenios::class;
 
    /**
     * @inheritdoc
     */
    public $searchClass = ConveniosSearch::class;

}

