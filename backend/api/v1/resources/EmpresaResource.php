<?php

namespace backend\api\v1\resources;
 
use backend\api\v1\models\Empresa;
use backend\api\v1\models\search\EmpresaSearch;
use tecnocen\roa\controllers\Resource;
use Yii;
use yii\helpers\VarDumper;
 
class EmpresaResource extends Resource
{
    public $idAttribute = 'id';
    /**
     * @inheritdoc
     */
    public $modelClass = Empresa::class;
 
    /**
     * @inheritdoc
     */
    public $searchClass = EmpresaSearch::class;


    /*public function actions()
    {
        $actions = parent::actions();
        unset( $actions['create'], $actions['update']);
        return $actions;
    }

    public function actionCreate(){


    	//VarDumper::dump($_POST,10,true);
    	//die();
		$model = new $this->modelClass();
        $datos = ['Empresa' => Yii::$app->request->post()];
        $model->load($datos);
       	$model->save();
       	Yii::$app->getResponse()->setStatusCode(201);
       	return $model;
    }*/
}

