<?php

namespace backend\api\v1\resources;
 
use backend\api\v1\models\Convenio;
use backend\api\v1\models\search\ConvenioSearch;
use tecnocen\roa\controllers\Resource;
use Yii;
use yii\helpers\VarDumper;
 
class ConvenioResource extends Resource
{
    public $idAttribute = 'id';
    /**
     * @inheritdoc
     */
    public $modelClass = Convenio::class;
 
    /**
     * @inheritdoc
     */
    public $searchClass = ConvenioSearch::class;




}

