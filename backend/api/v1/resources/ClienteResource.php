<?php

namespace backend\api\v1\resources;
 
use backend\api\v1\models\Cliente;
use backend\api\v1\models\search\ClienteSearch;
use tecnocen\roa\controllers\Resource;
use Yii;
use yii\helpers\VarDumper;
 
class ClienteResource extends Resource
{
    public $idAttribute = 'id';
    /**
     * @inheritdoc
     */
    public $modelClass = Cliente::class;
 
    /**
     * @inheritdoc
     */
    public $searchClass = ClienteSearch::class;




}

