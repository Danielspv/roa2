<?php

namespace backend\api\v1;

use tecnocen\roa\controllers\ProfileResource;
use tecnocen\roa\urlRules\SingleRecord as SingleRecordUrlRule;

class Version extends \tecnocen\roa\modules\ApiVersion
{
    /**
     * @inheritdoc
     */
    public $resources = [
        'profile' => [
            'class' => ProfileResource::class,
            'urlRule' => ['class' => SingleRecordUrlRule::class],
        ],
        'user',
        "empresa" => [
            'class' => \backend\api\v1\resources\EmpresaResource::class,
        ],
        'empresa/<empresa_id:\d+>/convenios' => [
            'class' => \backend\api\v1\resources\ConveniosResource::class,
        ],
        'convenio' => [
        'class' => \backend\api\v1\resources\ConvenioResource::class,
        ],
        'cliente' => [
        'class' => \backend\api\v1\resources\ClienteResource::class,
        ],
    ];

    /**
     * @inheritdoc
     */
    public $controllerNamespace = resources::class;
}
