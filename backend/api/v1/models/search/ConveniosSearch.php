<?php
 
namespace backend\api\v1\models\search;
 
use backend\api\v1\models\Convenios;
use tecnocen\roa\ResourceSearch;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
 
class ConveniosSearch extends Convenios implements ResourceSearch
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [[['empresa_id','nombre'],'safe']];

    }
 
    /**
     * @inheritdoc
     */
    public function search(array $params, $formName = '')
    {
        $this->load($params, $formName);
        if (!$this->validate()) {
            return null;
        }
 
        $query = static::find();
 
        $query->andFilterWhere(['like', 'nombre', $this->nombre]);
        $query->andFilterWhere(['=', 'empresa_id', $this->empresa_id]);
   
    
        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => ArrayHelper::getValue($params, 'registros', 20),
            ],
        ]);
    }
}