<?php
 
namespace backend\api\v1\models\search;
 
use backend\api\v1\models\Empresa;
use tecnocen\roa\ResourceSearch;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
 
class EmpresaSearch extends Empresa implements ResourceSearch
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [[['nombre'],'safe']];
    }
 
    /**
     * @inheritdoc
     */
    public function search(array $params, $formName = '')
    {
        $this->load($params, $formName);
        if (!$this->validate()) {
            return null;
        }
 
        $query = static::find();
 
        $query->andFilterWhere(['like', 'nombre', $this->nombre]);
   
 
 
        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => ArrayHelper::getValue($params, 'registros', 20),
            ],
        ]);
    }
}