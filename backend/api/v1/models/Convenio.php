<?php
 
 
namespace backend\api\v1\models;
 
use tecnocen\roa\behaviors\Slug;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\web\Link;
use yii\web\Linkable;
 
class Convenio extends \common\models\Convenio implements Linkable
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'slug' => [
                'class' => Slug::class,
                'idAttribute' => 'id',
                'resourceName' => 'convenio',
                'parentSlugRelation' => 'empresa',
                'checkAccess' => function ($params) {
                    if (isset($params['empresa_id'])
                        && $this->empresa_id != $params['empresa_id']
                    ) {
                        throw new \yii\web\NotFoundHttpException(
                            "Registro no asociado a la empresa {$params['empresa_id']}."
                        );
                    }
                },
            ],
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('now()'),
                'createdAtAttribute' => 'fecha_alta',
                'updatedAtAttribute' => 'fecha_actualizacion',
            ],
        ]);
    }
 
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresa()
    {
        return $this->hasOne(Empresa::className(), ['id' => 'empresa_id']);
    }
 
    /**
     * @inheritdoc
     */
    public function getLinks()
    {
        return array_merge($this->slugLinks, [
            'curies' => new Link([
               'name' => 'docs',
               'title' => 'Resource Documentation',
               'href' => 'http://swagger.com/demo/{rel}',
               'templated' => true,
            ]),
            'docs:self' => 'empresa/convenio',
         ]);
    }
 
    public function fields()
    {
        return [
            'id',
            'empresa_id',
            'nombre',
            'plazos',
            'periodicidad',
            'tasa',
            'tipo_saldo',
            'estatus',
        ];
    }
}