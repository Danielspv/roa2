<?php
 
 
namespace backend\api\v1\models;
 
use tecnocen\roa\behaviors\Slug;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\web\Linkable;
 
class Empresa extends \common\models\Empresa implements Linkable
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'slug' => [
                'class' => Slug::class,
                'resourceName' => 'empresa',
                'idAttribute' => 'id',
            ],
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('now()'),
                'createdAtAttribute' => 'fecha_alta',
                'updatedAtAttribute' => 'fecha_actualizacion',
            ],
 
        ]);
    }
 
    /**
     * @inheritdoc
     */
    public function getLinks()
    {
        return array_merge($this->slugLinks, [
                'curies' => new \yii\web\Link([
                    'name' => 'docs',
                    'title' => 'Resource Documentation',
                    'href' => 'http://swagger.com/demo/{rel}',
                    'templated' => true,
                ]),
                'docs:self' => 'Empresa',
                /*'convenios' => new Link([
                    'href' => Url::to(["empresa/{$this->id}/convenios"]),
                    'name' => 'convenios',
                    'title' => 'Lista de Convenios por Empresa',
                    'type' => 'application/json',
            ]),*/
        ]);
    }
 
    public function fields()
    {
        return [
            'id',
            'nombre',
            'estatus',
        ];
    }
}