<?php
 
 
namespace backend\api\v1\models;
 
use tecnocen\roa\behaviors\Slug;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\web\Link;
use yii\web\Linkable;
 
class Cliente extends \common\models\Cliente implements Linkable
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'slug' => [
                'class' => Slug::class,
                'idAttribute' => 'id',
                'resourceName' => 'cliente',
                'parentSlugRelation' => 'empresa',
                'checkAccess' => function ($params) {
                    if (isset($params['empresa_id'])
                        && $this->empresa_id != $params['empresa_id']
                    ) {
                        throw new \yii\web\NotFoundHttpException(
                            "Registro no asociado a la empresa {$params['empresa_id']}."
                        );
                    }
                },
            ],
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('now()'),
                'createdAtAttribute' => 'fecha_alta',
                'updatedAtAttribute' => 'fecha_actualizacion',
            ],
        ]);
    }
 
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresa()
    {
        return $this->hasOne(Empresa::className(), ['id' => 'empresa_id']);
    }

        public function getCreditos()
    {
        return $this->hasMany(Credito::className(), ['cliente_id' => 'id']);
    }
 
    /**
     * @inheritdoc
     */
    public function getLinks()
    {
        return array_merge($this->slugLinks, [
            'curies' => new Link([
               'name' => 'docs',
               'title' => 'Resource Documentation',
               'href' => 'http://swagger.com/demo/{rel}',
               'templated' => true,
            ]),
            'docs:self' => 'cliente/empresa',
         ]);
    }
 
    public function fields()
    {
        return [
            'id',
            'nombre',
            'ap_paterno',
            'ap_materno',
            'fecha_nacimiento',
            'rfc',
            'estatus',
            'empresa_id',
        ];
    }
}