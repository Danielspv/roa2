<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\VarDumper;
use common\models\Convenio;

/**
 * This is the model class for table "credito".
 *
 * @property int $id
 * @property int|null $cliente_id
 * @property int|null $convenio_id
 * @property string|null $folio
 * @property float|null $monto
 * @property int|null $plazos
 * @property int|null $periodicidad
 * @property float|null $tasa
 * @property float|null $descuento
 * @property float|null $total_pagar
 * @property string|null $fecha_alta
 * @property string|null $fecha_actualizacion
 *
 * @property Cliente $cliente
 * @property Convenio $convenio
 * @property CreditoPagos[] $creditoPagos
 */
class Credito extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'credito';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cliente_id', 'convenio_id', 'plazos', 'periodicidad'], 'integer'],
            [['monto', 'tasa', 'descuento', 'total_pagar'], 'number'],
            [['fecha_alta', 'fecha_actualizacion'], 'safe'],
            [['folio'], 'string', 'max' => 250],
            [['cliente_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cliente::className(), 'targetAttribute' => ['cliente_id' => 'id']],
            [['convenio_id'], 'exist', 'skipOnError' => true, 'targetClass' => Convenio::className(), 'targetAttribute' => ['convenio_id' => 'id']],
            [['folio'],'unique'],
            [['total_pagar'], 'filter','filter' => function($total_pagar){

                    $model->load(Yii::$app->request->post());
                    $nopagos= (int)$this->plazos;
                    $periodicidad =(int)$this->periodicidad;
                    $tasa = (double)$this->tasa;
                    $descuento = (double)$this->descuento;
                    $monto = (double)$this->monto;
                    $model->tipo_saldo = (int)$this->$model->convenio->tipo_saldo;

                        if($tiposaldo == 1){

                            if($periodicidad == 3){
                                $total_pagar = ($monto/4)*$tasa*$nopagos;
                            }else{
                                $total_pagar = ($monto/$periodicidad)*$tasa*$nopagos;
                            }

                        }

                        if($tiposaldo == 2){
                            $total_pagar = $decuento*$nopagos;
                            
                        }
                    return $total_pagar;
              
            }],
            [['descuento'], 'filter','filter' => function($descuento){

                    $modelConvenio = Convenio::find()
                    ->where(['id' => $this->convenio_id])
                    ->one();
                    $tiposaldo = $modelConvenio->tipo_saldo;
                    $nopagos= (int)$this->plazos;
                    $tasa =(double)$this->tasa;
                    $totalapagar = (double)$this->total_pagar;
                    $monto = (double)$this->monto;

                    if($tiposaldo == 1){
                        $descuento = $totalapagar/$nopagos;
                        
                    }

                    if($tiposaldo == 2){
                        $descuento = ($monto*$tasa)/[1-((1+$tasa)^-$nopagos)];
                        
                    }
                return $descuento;
            }],
        ];
    }

    public function calculartotal()
    {


        
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'fecha_alta',
                'updatedAtAttribute' => 'fecha_actualizacion',
                'value' => new Expression('NOW()'),
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cliente_id' => 'Nombre del cliente',
            'convenio_id' => 'Nombre del convenio',
            'folio' => 'Folio',
            'monto' => 'Monto',
            'plazos' => 'Plazos',
            'periodicidad' => 'Periodicidad',
            'tasa' => 'Tasa',
            'descuento' => 'Descuento',
            'total_pagar' => 'Total Pagar',
            'fecha_alta' => 'Fecha Alta',
            'fecha_actualizacion' => 'Fecha Actualizacion',
        ];
    }



    /**
     * Gets query for [[Cliente]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCliente()
    {
        return $this->hasOne(Cliente::className(), ['id' => 'cliente_id']);
    }

    /**
     * Gets query for [[Convenio]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getConvenio()
    {
        return $this->hasOne(Convenio::className(), ['id' => 'convenio_id']);
    }

    /**
     * Gets query for [[CreditoPagos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreditoPagos()
    {
        return $this->hasMany(CreditoPagos::className(), ['credito_id' => 'id']);
    }
}
