<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "credito_pagos".
 *
 * @property int $id
 * @property int|null $credito_id
 * @property int|null $numero_pago
 * @property float|null $monto
 * @property string|null $fecha_pago
 * @property int|null $estatus
 * @property string|null $fecha_alta
 * @property string|null $fecha_actualizacio
 *
 * @property Credito $credito
 */
class CreditoPagos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'credito_pagos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['credito_id', 'numero_pago', 'estatus'], 'integer'],
            [['monto'], 'number'],
            [['fecha_pago', 'fecha_alta', 'fecha_actualizacio'], 'safe'],
            [['credito_id'], 'exist', 'skipOnError' => true, 'targetClass' => Credito::className(), 'targetAttribute' => ['credito_id' => 'id']],
            [['estatus'],'default','value'=>1],
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'fecha_alta',
                'updatedAtAttribute' => 'fecha_actualizacio',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    public function crearPagos($idcredito,$noplazos,$total_pagar,$descuento,$fecha_alta){

        for ($i=1; $i <= $noplazos ; $i++) { 
            $modelCreditoPago = new self();
            $modelCreditoPago->credito_id=$idcredito;
            $modelCreditoPago->numero_pago=$i;
            $modelCreditoPago->monto=$descuento;
            if($noplazos== 12){
                $time = strtotime("+1 mont",$time);
                $final = date("Y-m-d",$time);
                $modelCreditoPago->fecha_pago = $final;

            }
            $modelCreditoPago->estatus=1;

            $modelCreditoPago->save(false);

        }


    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'credito_id' => 'Credito ID',
            'numero_pago' => 'Numero Pago',
            'monto' => 'Monto',
            'fecha_pago' => 'Fecha Pago',
            'estatus' => 'Estatus',
            'fecha_alta' => 'Fecha Alta',
            'fecha_actualizacio' => 'Fecha Actualizacio',
        ];
    }

    /**
     * Gets query for [[Credito]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCredito()
    {
        return $this->hasOne(Credito::className(), ['id' => 'credito_id']);
    }
}
 