<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\VarDumper;
use yii\helpers\ArrayHelper;


/**
 * This is the model class for table "convenio".
 *
 * @property int $id
 * @property int|null $empresa_id
 * @property string|null $nombre
 * @property int|null $plazos
 * @property int|null $periodicidad
 * @property float|null $tasa
 * @property int|null $tipo_saldo
 * @property int|null $estatus
 * @property string|null $fecha_alta
 * @property string|null $fecha_actualizacion
 *
 * @property Empresa $empresa
 * @property Credito[] $creditos
 */


class Convenio extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'convenio';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['empresa_id', 'plazos', 'periodicidad', 'tipo_saldo', 'estatus'], 'integer'],
            [['fecha_alta', 'fecha_actualizacion'], 'safe'],
            [['nombre'], 'string', 'max' => 250],
            [['empresa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Empresa::className(), 'targetAttribute' => ['empresa_id' => 'id']],
            [['nombre'],'unique'],
            [['empresa_id','plazos','periodicidad','nombre','tasa','tipo_saldo'],'required'],
            [['estatus'], 'default', 'value' => 1],
            [['tasa'],'number','max'=>4.8,'min'=>2.0],
            [['periodicidad'],'in','range'=>[1,2,3]],
            [['tipo_saldo'],'in','range'=>[1,2]],
            [['plazos'], 'filter','filter' => function($value){

                $periodicidad = (int)$this->periodicidad;
                
                if($periodicidad==1){ 
                    if($value!=12){
                        $this->addError('plazos','Solo se permiten 12 plazos');
                    }
                }
                
                if($periodicidad==2){
                    if($value!=24 && $value!=48 && $value!=72){
                        $this->addError('plazos','Solo se permiten 24,48 o 72 plazos');
                    }
                } 
                if($periodicidad==3){
                    if($value!=52){
                        $this->addError('plazos','Solo se permiten 52 plazos');
                    }
                }
                return $value; 
              
            }],

        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'fecha_alta',
                'updatedAtAttribute' => 'fecha_actualizacion',
                'value' => new Expression('NOW()'),
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'empresa_id' => 'Empresa',
            'nombre' => 'Nombre',
            'plazos' => 'Plazos',
            'periodicidad' => 'Periodicidad',
            'tasa' => 'Tasa',
            'tipo_saldo' => 'Tipo Saldo',
            'estatus' => 'Estatus',
            'fecha_alta' => 'Fecha Alta',
            'fecha_actualizacion' => 'Fecha Actualizacion',
        ];
    }

    /**
     * Gets query for [[Empresa]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresa()
    {
        return $this->hasOne(Empresa::className(), ['id' => 'empresa_id']);
    }

}
