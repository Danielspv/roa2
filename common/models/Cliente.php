<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;


/**
 * This is the model class for table "cliente".
 *
 * @property int $id
 * @property string|null $nombre
 * @property string|null $ap_paterno
 * @property string|null $ap_materno
 * @property string|null $fecha_nacimiento
 * @property string|null $rfc
 * @property int|null $estatus
 * @property int|null $empresa_id
 * @property string|null $fecha_alta
 * @property string|null $fecha_actualizacion
 *
 * @property Empresa $empresa
 * @property Credito[] $creditos
 */
class Cliente extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cliente';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {


        return [
            [['nombre','ap_paterno','ap_materno','fecha_nacimiento','rfc'], 'required' ],
            [['fecha_nacimiento', 'fecha_alta', 'fecha_actualizacion'], 'safe'],
            [['estatus', 'empresa_id'], 'integer'],
            [['nombre', 'ap_paterno', 'ap_materno'], 'string', 'max' => 250],
            [['rfc'], 'string', 'max' => 13],
            [['empresa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Empresa::className(), 'targetAttribute' => ['empresa_id' => 'id']],
            [['rfc'], 'unique'],
            ['estatus', 'default', 'value' => 1],
            [['nombre'], 'unique', 'targetAttribute' => ['nombre', 'ap_paterno', 'ap_materno']],
            [['fecha_nacimiento'], 'date', 'format' => 'php:Y-m-d', 'max' => strtotime('today -18 years'), 'tooBig' => 'La edad minima es de 18 años'],
            /*
            [['fecha_nacimiento'], 'filter','filter' => function($value){
                
                $value = strtotime($value);
                
                $datetoday = strtotime('today -18 years');
                
                if($value > $datetoday)  {
                    $this->addError('fecha_nacimiento' ,'No es mayor de edad');     
                }
            }],*/

        ];

    }


    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'fecha_alta',
                'updatedAtAttribute' => 'fecha_actualizacion',
                'value' => new Expression('NOW()'),
            ],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'ap_paterno' => 'Apellido Paterno',
            'ap_materno' => 'Apellido Materno',
            'fecha_nacimiento' => 'Fecha Nacimiento',
            'rfc' => 'Rfc',
            'estatus' => 'Estatus',
            'empresa_id' => 'Empresa',
            'fecha_alta' => 'Fecha Alta',
            'fecha_actualizacion' => 'Fecha Actualizacion',
        ];
    }

    /**
     * Gets query for [[Empresa]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompania()
    {
        return $this->hasOne(Empresa::className(), ['id' => 'empresa_id']);
    }

    /**
     * Gets query for [[Creditos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreditos()
    {
        return $this->hasMany(Credito::className(), ['cliente_id' => 'id']);
    }

    public function getClientes()
    {
        $querycliente = Cliente::find()
        ->select(['id','concat(nombre,SPACE(1),ap_paterno,SPACE(1),ap_materno) as nombre'])
        ->andWhere(['estatus'=>1])
        ->asArray()
        ->all();
        return ArrayHelper::map($querycliente,'id','nombre'); 
    }
    
}
