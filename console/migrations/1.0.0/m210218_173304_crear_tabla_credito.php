<?php

use yii\db\Migration;

/**
 * Class m210209_173304_crear_tabla_credito
 */
class m210218_173304_crear_tabla_credito extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%credito}}', [
            'id' => $this->primaryKey(),
            'cliente_id' => $this->integer(11),
            'convenio_id' => $this->integer(11),
            'folio' => $this->string(250),
            'monto' => $this->decimal(10,2),
            'plazos' => $this->integer(2),
            'periodicidad' => $this->integer(3),
            'tasa' => $this->decimal(3,2),
            'descuento' => $this->decimal(10,2),
            'total_pagar' => $this->decimal(10,2),
            'fecha_alta'  => $this->datetime(),
            'fecha_actualizacion' => $this->datetime(),
        ], $tableOptions);

        $this->addForeignKey(
            'fk-credito-cliente_id',
            'credito',
            'cliente_id',
            'cliente',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-credito-convenio_id',
            'credito',
            'convenio_id',
            'convenio',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210218_173304_crear_tabla_credito cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210209_173304_crear_tabla_credito cannot be reverted.\n";

        return false;
    }
    */
}
