<?php

use yii\db\Migration;

/**
 * Class m210209_164934_crear_tabla_empresa
 */
class m210218_164934_crear_tabla_empresa extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        //Crear la tabla empresa
         $this->createTable('{{%empresa}}', [
            'id' => $this->primaryKey(),
            'nombre' => $this->string(250),
            'estatus' => $this->integer(2),
            'fecha_alta' => $this->datetime(),
            'fecha_actualizacion' => $this->datetime(),
        ], $tableOptions);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210218_164934_crear_tabla_empresa cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210209_164934_crear_tabla_empresa cannot be reverted.\n";

        return false;
    }
    */
}
