<?php

use yii\db\Migration;

/**
 * Class m210209_165458_crear_tabla_convenio
 */
class m210218_165458_crear_tabla_convenio extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
    //Crea la tabla convenio
        $this->createTable('{{%convenio}}', [
            'id' => $this->primaryKey(),
            'empresa_id' => $this->integer(11),
            'nombre' => $this->string(250),
            'plazos' => $this->integer(2),
            'periodicidad' => $this->integer(3),
            'tasa' => $this->decimal(3,2),
            'tipo_saldo' => $this->integer(1),
            'estatus' => $this->integer(2),
            'fecha_alta' => $this->datetime(),
            'fecha_actualizacion' => $this->datetime(),
        ], $tableOptions);

        $this->addForeignKey(
        'fk-convenio-empresa_id',
        'convenio',
        'empresa_id',
        'empresa',
        'id',
        'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210218_165458_crear_tabla_convenio cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210209_165458_crear_tabla_convenio cannot be reverted.\n";

        return false;
    }
    */
}
